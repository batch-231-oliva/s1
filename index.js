alert("Hi!")

// Global Objects
// Arrays

let students = [
	"Tony",
	"Peter",
	"Wanda",
	"Vision",
	"Loki"
];
console.log(students);

// What array method can we use to add an item at the end of the array?
students.push("Thor");
console.log(students);

// What array method can we use to add an item at the start of the array?
students.unshift("Steve");
console.log(students);

// What array method does the opposite of push method()?
students.pop();
console.log(students);

// What array method does te opposite of unshift()?
students.shift();
console.log(students);

// What is the difference between splice() and slice()
	// .splice() -  removes and adds items from starting index (Mutator methods)
	// .slice() - copies a portion from starting index and retunrs new array from it (Non-Mutator methods)

// Another kind of array methods?
	// Iterator methods - loops over the items of an array

// forEach() - loops over items an array and repeats a user-defined function
// map() -  loops over items in an array and repeats a user-defined function AND returns a new array
// every() - loops and checks if all items satisfy a given condition, returns a boolean value

let arrNum = [ 15, 20, 25, 30, 11]

// check if every item in arrNum is divisible by 5
let allDivisible;

arrNum.forEach(num => {
	if(num % 5 === 0){
		console.log(`${num} is divisible by 5`)
	} else {
		allDivisible = false;
	}
	// However, can forEach() retunr data that will tell us IF all numbers/ items in our arrNum array is divisible by 5?
})
console.log(allDivisible);

arrNum.pop();
arrNum.push(35);
console.log(arrNum);

let divisibleBy5 = arrNum.every(num => {
	console.log(num);
	return (num % 5 === 0)
})
console.log(divisibleBy5);
// result: true

// Math
// mathematical constants
// 8 pre-defined properties which can be called via syntax Math.property (note that properties are case-sensitive)
console.log(Math);
console.log(Math.E); // Euler's number
console.log(Math.PI); // PI
console.log(Math.SQRT2); // square root of 2
console.log(Math.SQRT1_2); // square root of 1/2
console.log(Math.LN2); // natural logarithm of 2
console.log(Math.LN10); // natural logarithm of 10
console.log(Math.LOG2E); // base 2 logarithm of E
console.log(Math.LOG10E); // base 10 logarithm of E

// methods for rounding a number to an integer
console.log(Math.round(Math.PI)); // rounds to a nearest integer; result: 3
console.log(Math.ceil(Math.PI)); // rounds UP to a nearest integer; resutlt: 4
console.log(Math.floor(Math.PI)); // rounds DOWN to a nearest integer; result: 3
console.log(Math.trunc(Math.PI)); // returns only the integer part (ES6 update); result: 3

// method for returning the square root of a number
console.log(Math.sqrt(3.14)); // result: 1.77

// lowest value in a list of arguments
console.log(Math.min(-4, -3, -2, -1, 0, 1, 2, 3, 4)) ; // result: -4

// highest value in a list arguments
console.log(Math.max(-4, -3, -2, -1, 0, 1, 2, 3, 4)) ; // result: 4


// Activity - Function Coding

students = ["John", "Joe", "Jane", "Jessie"];

// 1. addToEnd function

function addToEnd (students, student) {
	
	if (typeof student == 'string'){
		students.push(student)
		return console.log(students)

	} else {
		return console.log("error - can only add strings to an array")
	};
};

addToEnd(students, "Ryan");
addToEnd(students, 045);


// 2. addToStart

function addToStart (students, student) {

	if (typeof student == 'string'){
		students.unshift(student)
		return console.log(students)

	} else {
		return console.log("error - can only add strings to an array")
	};
};

addToStart(students, "Tess");
addToStart(students, 033);


// 3. elementChecker

function elemetChecker (students, student) {
	let checker = students.some( name => {
		return (name === student)
	})

	if (checker) {
		return console.log(true)

	} else {
	return console.log("error - passed in array is empty")
	}
}

elemetChecker(students, "Jane");
elemetChecker([], "Jane");


// 5. checkAllStringsEnding

function checkAllStringsEnding (students, char) {
	if (students.length == 0) {
		return console.log("error - array must NOT be empty")
	}

	if (typeof char != 'string') {
		return console.log("error - 2nd argument must be of data type string")
	}

	if (char.length > 1) {
		return console.log("error - 2nd argument must be a single character")
	} else {
		students.some(element => {
			if (typeof element != 'string') {
				return console.log("error - all array elements must be strings")
			} else {
				students.every(lastChar => {
					if (lastChar[length] == char) {
						return console.log(true)
						
					} else {
						return console.log(false)
						
					}
				})
			} 
		})
	}		
}

console.log(students)
checkAllStringsEnding(students, "e");
checkAllStringsEnding([], "e");
checkAllStringsEnding(["Jane", 2], "e");
checkAllStringsEnding(students, "el");
checkAllStringsEnding(students, 4);


// 5. stringLengthSorter


function stringLengthShorter (students) {

const sortedStudents = students.sort((a,b) => a.length - b.length);

let elementCheck = students.every(element => {
	if (typeof element == 'string') {
		return true
		
	} else {
		return false
	}
})

if (elementCheck) {
	return console.log(sortedStudents)
}
 else{
 	return console.log ("error - all array elements must be strings")
 }

}

stringLengthShorter(students)
stringLengthShorter([037, "John", 039, "Jane"])